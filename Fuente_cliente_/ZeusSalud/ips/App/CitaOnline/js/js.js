// JavaScript Document
var parametros='';
function ajaxFunction() {
  var xmlHttp;
  
  try {
   
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
  } catch (e) {
    
    try {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
    } catch (e) {
      
	  try {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
      } catch (e) {
        alert("Tu navegador no soporta AJAX!");
        return false;
      }}}
}

function ajaxPOST(_pagina, contenedor,metodo,loading,param){
	
	try{
		//if(loading!=''){
			document.getElementById('fountainTextG').style.visibility="visible";
		//}
	}catch(e){}
	var f = document.getElementById("formulario");
	var ajax;
	ajax = ajaxFunction();
	ajax.open("POST", _pagina, true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.onreadystatechange = function() {
		if (ajax.readyState == 4) {
			if(contenedor!=''){
				document.getElementById(contenedor).innerHTML=ajax.responseText; 
			}
			//if(loading!=''){
				document.getElementById('fountainTextG').style.visibility="hidden";
			//}
			if(metodo!=''){
				setTimeout(metodo,0);
			}
		}
	}
	ajax.send(param);
}

function get(id){
	return document.getElementById(id);
}

function SolicitarCitas(){
	paciente=get('existePaciente').value;
	asunto=get('asunto').value;
	if(paciente>0){
		/*$("#tipoIdentificacion").attr("disabled", "disabled");
		$("#identificacion").attr("disabled", "disabled");
		$("#asunto").attr("disabled", "disabled");
		$("#btnEntrar").attr("disabled", "disabled");*/
		
		ajaxPOST("Vistas/solicitarCitas.php",'contentPane','setBtnConsultar()','','autoid='+paciente+'&asunto='+asunto);
	}else{
		$.notify("Paciente no existente.", "error");

	}
}

function setCalendar(calendar,num){
	var myDate = new Date();
	myDate.setMonth(myDate.getMonth() + num, 1);
	
	$( "#"+calendar ).datepicker({
			inline: true,
			defaultDate:myDate,

			onSelect: function(dateText, inst) { 
			  get('fecha').value=dateText;
			  consultarDisponibilidadMedica();
			},
			 onChangeMonthYear: function(year, month, inst) {
				var myDate = new Date();
				myDate.setMonth(myDate.getMonth() + 3, 1);
				$('#'+calendar).datepicker('option', {defaultDate: myDate}); 
				/*$('#datepicker2').datepicker({
				inline: true,
				defaultDate:myDate});*/
			},
			
			
			beforeShowDay: function(date) {
				var current = $.datepicker.formatDate('yy-m-dd', date);	
				return [true, 'ui-state-hover', ''];	
				//return jQuery.inArray(current, events) == -1 ? [true, ''] : [true, 'ui-state-hover ui-festivo', ''];
			}

		});
	
	$(".ui-widget").css("fontSize", "0.7em");	
	$('#'+calendar).datepicker('option', {dateFormat: 'yy/mm/dd'}); 
}

function setBtnConsultar(){
	setCalendar('calendar1',0);
	setCalendar('calendar2',1);
	setCalendar('calendar3',2);

	$("#btnConsultarDisponibilidad").click(function (data){
		if(formValidate('formConsultarDisponibilidad')){
			cambiarPestania("liAC");
			consultarDisponibilidadMedica();
		}
	});
	setPestanias();
	consultarCitasPacientes();
}

function ocultarDivTime(nameDivRes){
	setTimeout('$("#'+nameDivRes+'").hide(1000,function(){get("'+nameDivRes+'").innerHTML="";$("#'+nameDivRes+'").show();});', 5000);	
}


function setPestanias(){
	$(".tab_content").hide(); //Esconde todo el contenido 
	$("ul.tabs li:first").addClass("active").show(); //Activa la primera 
	$(".tab_content:first").show(); //Muestra el contenido de la primera tab //On Click Event 
	$("ul.tabs li").click(function() { $("ul.tabs li").removeClass("active"); //Elimina las clases activas 
	$(this).addClass("active"); //Agrega la clase activa a la tab seleccionada 
	$(".tab_content").hide(); //Esconde todo el contenido de la tab 
	var activeTab = $(this).find("a").attr("href"); //Encuentra el valor del atributo href para identificar la tab activa + el contenido 
	$(activeTab).fadeIn(); //Agrega efecto de transición (fade) en el contenido activo return false; 
	}); 
}

function cambiarPestania(li){
	$("#"+li).click();
}


function consultarDisponibilidadMedica(){
	if(formValidate('formConsultarDisponibilidad')){
		var medico=get('medico').value;
		var fecha=get('fecha').value;
		ajaxPOST("Controladores/Citas/Citas.php",'DivDisponibilidadCitas','consultarCitasPacientes();','','Op=listarProgramacion&medico='+medico+'&fecha='+fecha);
	}
}

function consultarCitasPacientes(){
	var autoid=get('autoid').value;
	ajaxPOST("Controladores/Citas/Citas.php",'divCitasAsignadas','','','Op=consultarCitasPaciente&autoid='+autoid);
}


function selecionarHora(hora,meridiano,fecha_cita,codMedico,nomMedico,id_programacion){
	if(confirm("Seguro desea apartar la cita?")){
		var autoid=get('autoid').value;
		var asunto=get('asunto').value;
		ajaxPOST("Controladores/Citas/Citas.php",'divMensajeCita','consultarDisponibilidadMedica();ocultarDivTime("divMensajeCita");cambiarPestania("liCC");','','Op=apartar_cita&hora='+hora+'&meridiano='+meridiano+'&fecha_cita='+fecha_cita+'&codMedico='+codMedico+'&nomMedico='+nomMedico+'&id_programacion='+id_programacion+'&autoid='+autoid+'&asunto='+asunto);
	}
}


function SendData(formulario,urlreceptor,contenedor,callback,param){
	if(formValidate(formulario)){
		ajaxPOST(urlreceptor,contenedor,callback,'',param+'&'+$("#"+formulario).serialize());
	}
}

function formValidate(formulario){
	var retorno=true;
	 $("#"+formulario).find(':input').each(function() {
		  var elemento= this;
		  if($(this).hasClass('require')&&$.trim(elemento.value)==''){
			$(this).addClass("requerido");
			$(this).notify("Campo Requerido");
			retorno=false;
			return false;
		  }else{
			$(this).removeClass("requerido");
		  }
	 });
	 return retorno;
}